package com.logistoffice.autooffice;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;


import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static android.location.LocationManager.GPS_PROVIDER;
import static android.location.LocationManager.NETWORK_PROVIDER;


public class MainActivity extends  ActionBarActivity implements LocationListener {
public int gpson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        TextView tv = (TextView) findViewById(R.id.textView2);
        //EditText etResponse = (EditText) findViewById(R.id.editText);
        TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(this);
        String imsiSIM1 = telephonyInfo.getImsiSIM1();

        tv.setText(" ВАШ ID : " + imsiSIM1 + "\n");
        String url = "http://logistoffice.com/gpsoffice/get_user.php?imei=" + imsiSIM1;
//String url="http://api.androidhive.info/contacts";
        if (isConnected()) {
            // tv.setBackgroundColor(0xFF00CC00);
            tv.setText(" ВАШ ID : " + imsiSIM1 + "\n" + "нажмите SMS для отправки");

            new HttpAsyncTask().execute(url);

        } else {
            tv.setText("Подключение к интернет отсутствует");
        }
        // show response on the EditText etResponse

            final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 100; // 0 meters
            final long MIN_TIME_BW_UPDATES = 1000 * 60 * 4; // 4 minute
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (!locationManager.isProviderEnabled(GPS_PROVIDER) ||
                    !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                // Build the alert dialog
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Сервис определения местоположения выключен");
                builder.setMessage("Пожалуйста, включите сервис местоположения и GPS");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Show location settings when the user acknowledges the alert dialog
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                });
                Dialog alertDialog = builder.create();
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.show();
            }

            locationManager.requestLocationUpdates(NETWORK_PROVIDER, MIN_TIME_BW_UPDATES,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);


    }
        @Override

public void onBackPressed() {

}
    @Override
    protected void onResume() {
        super.onResume();
        TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(this);
        String imsiSIM1 = telephonyInfo.getImsiSIM1();
        String url="http://logistoffice.com/gpsoffice/get_user.php?imei=" + imsiSIM1;
        TextView tv = (TextView) findViewById(R.id.textView2);
        if(isConnected()){
            // tv.setBackgroundColor(0xFF00CC00);
            tv.setText(" ВАШ ID : " + imsiSIM1 + "\n" + "нажмите SMS для отправки");
            new HttpAsyncTask().execute(url);
        }
        else{
            tv.setText("Подключение к интернет отсутствует");
        }


    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.send_sms) {
            TextView tv = (TextView) findViewById(R.id.textView2);
            String sms_body = String.valueOf(tv.getText());
            sms_body = sms_body.substring(10, 26);
            TextView tv1 = (TextView) findViewById(R.id.name);
            String name = String.valueOf(tv1.getText());
            sms_body = "ID:" + sms_body + ", " + name;
            try {
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.putExtra("sms_body", sms_body);
                sendIntent.setType("vnd.android-dir/mms-sms");
                startActivity(sendIntent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(),
                        "SMS faild, please try again later!",
                        Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }

        }
        if (id == R.id.action_settings) {
            TextView name = (TextView) findViewById(R.id.name);
            TextView car_number = (TextView) findViewById(R.id.car_number);
            TextView car_volume = (TextView) findViewById(R.id.car_volume);
            TextView car_capacity = (TextView) findViewById(R.id.car_capacity);
            TextView car_type = (TextView) findViewById(R.id.car_type);
            TextView phone = (TextView) findViewById(R.id.phone);
            Intent intent = new Intent(this, EditUser.class);

                    // указываем первым параметром ключ, а второе значение
                    // по ключу мы будем получать значение с Intent
                    intent.putExtra("name", name.getText().toString());
                    intent.putExtra("car_number", car_number.getText().toString());
            intent.putExtra("car_volume", car_volume.getText().toString());
            intent.putExtra("car_capacity", car_capacity.getText().toString());
            intent.putExtra("car_type", car_type.getText().toString());
            intent.putExtra("phone", phone.getText().toString());
                    // показываем новое Activity
                    startActivity(intent);
                               }


        if (id == R.id.exit) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Выход из программы");
            builder.setMessage("Вы уверенны, что хотите закрыть приложение? После выхода, фиксация местоположения прекратится.");
            builder.setIcon(android.R.drawable.ic_dialog_alert);
            builder.setPositiveButton("ДА", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    //Yes button clicked, do something
                    finish();
                    System.exit(0);
                }
            }).setNegativeButton("НЕТ", null);
            builder.show();

        }
        if (id == R.id.change_status) {
            startActivity(new Intent(getApplicationContext(), status.class));
        }
        if (id == R.id.clear_data) {
            TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(this);
            String ims = telephonyInfo.getImsiSIM1();
            String url1 = "http://logistoffice.com/gpsoffice/update_gps.php?imei=" + ims;
            url1 = url1 + "&x1=";
            url1 = url1 + "&y1=";
            new HttpAsyncTask4().execute(url1);
            onResume();
        }
        if (id == R.id.GPS_ON) {
            TextView gps_status = (TextView) findViewById(R.id.gps_status);
            CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox);

            if (!checkBox.isChecked()) {
                checkBox.setChecked(true);
                checkBox.setText("Выключить GPS мониторинг");
                gps_status.setText("Ваше местоположение определяется по GPS");
                final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 100; // 100 meters
                final long MIN_TIME_BW_UPDATES = 1000 * 60 * 4; // 4 minute
                LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                if(!locationManager.isProviderEnabled(GPS_PROVIDER) ||
                        !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    // Build the alert dialog
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Сервис определения местоположения выключен");
                    builder.setMessage("Пожалуйста, включите сервис местоположения и GPS");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // Show location settings when the user acknowledges the alert dialog
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent);
                        }
                    });
                    Dialog alertDialog = builder.create();
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.show();
                }

                locationManager.requestLocationUpdates(GPS_PROVIDER, MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                gpson = 1;

                //  double  latitude =(location.getLatitude());
                //  double  longitude =(location.getLongitude());

            }
            else
            {
                LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                locationManager.removeUpdates(this);
                checkBox.setChecked(false);
                checkBox.setText("Включить GPS мониторинг");
                gps_status.setText("Ваше местоположение определяется сетью");
                final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 100; // 100 meters
                final long MIN_TIME_BW_UPDATES = 1000 * 60 * 4; // 4 minute
                if(!locationManager.isProviderEnabled(GPS_PROVIDER) ||
                        !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    // Build the alert dialog
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Сервис определения местоположения выключен");
                    builder.setMessage("Пожалуйста, включите сервис местоположения и GPS");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // Show location settings when the user acknowledges the alert dialog
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent);
                        }
                    });
                    Dialog alertDialog = builder.create();
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.show();
                }

                locationManager.requestLocationUpdates(NETWORK_PROVIDER, MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                gpson = 0;

                //  double  latitude =(location.getLatitude());
                //  double  longitude =(location.getLongitude());
            }

        }
        return super.onOptionsItemSelected(item);
    }
    public void click(View view) {

               }

    public static String GET(String url){
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            InputStream inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    // convert inputstream to String
    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line;
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    // check network connection
    public boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    @Override
    public void onLocationChanged(Location location) {

        double  latitude =(location.getLatitude());
        double  longitude =(location.getLongitude());

        TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(this);
        String ims = telephonyInfo.getImsiSIM1();

        String url1 = "http://logistoffice.com/gpsoffice/update_gps.php?imei=" + ims;
        url1 = url1 + "&x1=" + latitude;
        url1 = url1 + "&y1=" + longitude;

        new HttpAsyncTask4().execute(url1);
        onResume();

    }



    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);

        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
           // Toast.makeText(getBaseContext(), "Данные получены", Toast.LENGTH_SHORT).show();

            //etResponse.setText(result);

            try {
                JSONObject jObject = new JSONObject(result);
                JSONArray jArray = jObject.getJSONArray("transports");
                TextView name = (TextView) findViewById(R.id.name);
                TextView car_number = (TextView) findViewById(R.id.car_number);
                TextView car_volume = (TextView) findViewById(R.id.car_volume);
                TextView car_capacity = (TextView) findViewById(R.id.car_capacity);
                TextView car_type = (TextView) findViewById(R.id.car_type);
                TextView phone = (TextView) findViewById(R.id.phone);
                TextView status = (TextView) findViewById(R.id.status);
                TextView imei = (TextView) findViewById(R.id.textView2);

                Button register = (Button) findViewById(R.id.button3);

                      JSONObject oneObject = jArray.getJSONObject(0);
                        // Pulling items from the array
                        String oneObjectsItem1 = oneObject.getString("name");
                        String oneObjectsItem2 = oneObject.getString("car_number");
                        String oneObjectsItem3 = oneObject.getString("car_volume");
                        String oneObjectsItem4 = oneObject.getString("car_capacity");
                        String oneObjectsItem5 = oneObject.getString("car_type");
                        String oneObjectsItem6 = oneObject.getString("phone");
                        String oneObjectsItem7 = oneObject.getString("status");
                String oneObjectsItem8 = oneObject.getString("imei");
                String oneObjectsItem9 = oneObject.getString("arrivalcity");
                String oneObjectsItem10 = oneObject.getString("arrivaldate");
                String oneObjectsItem11 = oneObject.getString("date");
                String oneObjectsItem12 = oneObject.getString("fulladdress");

if (oneObjectsItem8.isEmpty()) {
imei.setBackgroundColor(0xFF00CC00);

    imei.setText("Вы новый пользователь - пройдите регистрацию");
    register.setVisibility(View.VISIBLE);
}
                else
{
    imei.setBackgroundColor(0x00000000);
    imei.setText(" ВАШ ID : " + oneObjectsItem8 + "\n" + "нажмите SMS для отправки");
    register.setVisibility(View.INVISIBLE);
}

                        name.setText("Водитель: " + oneObjectsItem1);
                        car_number.setText("Номер автомобиля: " + oneObjectsItem2);
                        car_volume.setText("Объем автомобиля (м3): " + oneObjectsItem3);
                        car_capacity.setText("Тоннаж автомобиля (т): " + oneObjectsItem4);
                        car_type.setText("Тип автомобиля: " + oneObjectsItem5);
                        phone.setText("Номер телефона: " + oneObjectsItem6);
if ("01-01-1970".equals(oneObjectsItem10)) {
    status.setText("Статус: " + oneObjectsItem7);
}
else
{
    status.setText("Статус: " + oneObjectsItem7 + " в " + oneObjectsItem9 + " до " + oneObjectsItem10);
}
           if (oneObjectsItem12.isEmpty()) {
               oneObjectsItem12="нет данных";
               TextView gps1 = (TextView) findViewById(R.id.gps1);
               TextView gps = (TextView) findViewById(R.id.gps);
               gps1.setText("\n" + "ВАШЕ ПОСЛЕДНЕЕ МЕСТОНАХОЖДЕНИЕ:");
               gps.setText(oneObjectsItem12);
           }
                else{


                TextView gps1 = (TextView) findViewById(R.id.gps1);
                TextView gps = (TextView) findViewById(R.id.gps);
                gps1.setText("\n" + "ВАШЕ ПОСЛЕДНЕЕ МЕСТОНАХОЖДЕНИЕ:");
                gps.setText(oneObjectsItem12 + "\n" +
                        "по состоянию на " + oneObjectsItem11);
           }
            } catch (JSONException e) {
                e.printStackTrace();

            }


         //   JSONArray jArray = jObject.getJSONArray("__metadata");

        }


    }
    public void register(View view) {
               startActivity(new Intent(getApplicationContext(), NewUser.class));



    }
    private class HttpAsyncTask4 extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            return GET4(urls[0]);

        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {

        }


    }

    public static String GET4(String url){

        String result = null;
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            InputStream     inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }
    public void send_sms(View view) {
        TextView tv = (TextView) findViewById(R.id.textView2);
        String sms_body = String.valueOf(tv.getText());
        sms_body = sms_body.substring(10, 26);
        TextView tv1 = (TextView) findViewById(R.id.name);
       String name = String.valueOf(tv1.getText());
        sms_body = "ID:" + sms_body + ", " + name;
        try {
            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
            sendIntent.putExtra("sms_body", sms_body);
            sendIntent.setType("vnd.android-dir/mms-sms");
            startActivity(sendIntent);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(),
                    "SMS faild, please try again later!",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

}



