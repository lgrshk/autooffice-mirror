package com.logistoffice.autooffice;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


public class EditUser extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);
        TextView tv = (TextView) findViewById(R.id.IMEI);
        //EditText etResponse = (EditText) findViewById(R.id.editText);
        TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(this);
        String imsiSIM1 = telephonyInfo.getImsiSIM1();
        tv.setText(" ВАШ ID : " + imsiSIM1 + "\n");

        String sname = getIntent().getStringExtra("name");
        String scar_number = getIntent().getStringExtra("car_number");
        String scar_volume = getIntent().getStringExtra("car_volume");
        String scar_capacity = getIntent().getStringExtra("car_capacity");
        String scar_type = getIntent().getStringExtra("car_type");
        String sphone = getIntent().getStringExtra("phone");

        EditText name = (EditText) findViewById(R.id.editText);
        EditText car_number = (EditText) findViewById(R.id.editText2);
        EditText car_volume = (EditText) findViewById(R.id.editText3);
        EditText car_capacity = (EditText) findViewById(R.id.editText4);
        EditText car_type = (EditText) findViewById(R.id.editText5);
        EditText phone = (EditText) findViewById(R.id.editText6);
        car_number.setText(scar_number.substring(18));
       name.setText(sname.substring(10));
        car_volume.setText(scar_volume.substring(23));
       car_capacity.setText(scar_capacity.substring(23));
        car_type.setText(scar_type.substring(16));
        phone.setText(sphone.substring(16));



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_user, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    // convert inputstream to String
    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line;
        String result=null;
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    // check network connection

    public void save_user(View view) throws UnsupportedEncodingException {
        TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(this);
        String ims = telephonyInfo.getImsiSIM1();

        EditText name = (EditText) findViewById(R.id.editText);
        EditText car_number = (EditText) findViewById(R.id.editText2);
        EditText car_volume = (EditText) findViewById(R.id.editText3);
        EditText car_capacity = (EditText) findViewById(R.id.editText4);
        EditText car_type = (EditText) findViewById(R.id.editText5);
        EditText phone = (EditText) findViewById(R.id.editText6);
        String name1 = URLEncoder.encode(String.valueOf(name.getText()), "utf-8");
        String car_number1 = URLEncoder.encode(String.valueOf(car_number.getText()), "utf-8");
        String car_type1 = URLEncoder.encode(String.valueOf(car_type.getText()), "utf-8");
        String car_volume1 = URLEncoder.encode(String.valueOf(car_volume.getText()), "utf-8");
        String car_capacity1 = URLEncoder.encode(String.valueOf(car_capacity.getText()), "utf-8");
        String phone1 = URLEncoder.encode(String.valueOf(phone.getText()), "utf-8");
        if (name1.equals("")) {
            Toast.makeText(getBaseContext(), "Не указано ФИО", Toast.LENGTH_SHORT).show();
            return;
        }
        if (car_number1.equals("")) {
            Toast.makeText(getBaseContext(), "Не указан гос номер авто", Toast.LENGTH_SHORT).show();
            return;
        }
        if (car_volume1.equals("")) {
            Toast.makeText(getBaseContext(), "Не указано бъем авто", Toast.LENGTH_SHORT).show();
            return;
        }
        if (car_capacity1.equals("")) {
            Toast.makeText(getBaseContext(), "Не указан тоннаж авто", Toast.LENGTH_SHORT).show();
            return;
        }
        if (car_type1.equals("")) {
            Toast.makeText(getBaseContext(), "Не указан тип авто", Toast.LENGTH_SHORT).show();
            return;
        }
        if (phone1.equals("")) {
            Toast.makeText(getBaseContext(), "Не указан телефон водителя", Toast.LENGTH_SHORT).show();
            return;
        }
        String url1 = "http://logistoffice.com/gpsoffice/update_user.php?name=" + name1;
        url1 = url1 + "&car_number=" + car_number1;
        url1 = url1 + "&car_volume=" + car_volume1;
        url1 = url1 + "&car_capacity=" + car_capacity1;
        url1 = url1 + "&car_type=" + car_type1;
        url1 = url1 + "&phone=" + phone1;
        url1 = url1 + "&status=Свободен";
        url1 = url1 + "&imei=" + ims;
        //Toast.makeText(getBaseContext(), url1, Toast.LENGTH_LONG).show();
        Toast.makeText(getBaseContext(), "Идет сохранение...", Toast.LENGTH_SHORT).show();

       // HttpAsyncTask().cancel(true);
        new HttpAsyncTask2().execute(url1);
    }

    private class HttpAsyncTask2 extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

             return GET2(urls[0]);
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
               // Toast.makeText(getBaseContext(),result, Toast.LENGTH_LONG).show();
                finish();
            }
            else {
                Toast.makeText(getBaseContext(), "Данные не сохранены \n Проверьте подключение к интернет", Toast.LENGTH_LONG).show();
            }

        }


    }

    public static String GET2(String url){

        String result=null;
        try {

            // create HttpClient
            HttpClient httpclient2 = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse2 = httpclient2.execute(new HttpPost(url));

            // receive response as inputStream
            InputStream    inputStream = httpResponse2.getEntity().getContent();

            // convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }
}


