package com.logistoffice.autooffice;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class GPS_Location extends Activity implements LocationListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gps__location);

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; // 0 meters
        final long MIN_TIME_BW_UPDATES = 1000 * 60 * 4; // 1 minute

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

      //  double  latitude =(location.getLatitude());
      //  double  longitude =(location.getLongitude());
        TextView tv = (TextView) findViewById(R.id.name);
        tv.setText("ВАШИ КООРДИНАТЫ:" + "\n" +
                "X : " + "\n" +
                "Y : " + "\n");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onLocationChanged(Location location) {
        // TODO Auto-generated method stub

        double  latitude =(location.getLatitude());
        double  longitude =(location.getLongitude());

        TextView tv = (TextView) findViewById(R.id.name);
        tv.setText("ВАШИ КООРДИНАТЫ:" + "\n" +
                "X : " + latitude + "\n" +
                "Y : " + longitude + "\n");


        TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(this);
        String ims = telephonyInfo.getImsiSIM1();

        String url1 = "http://logistoffice.com/gpsoffice/update_gps.php?imei=" + ims;
        url1 = url1 + "&x1=" + latitude;
        url1 = url1 + "&y1=" + longitude;

        //Toast.makeText(getBaseContext(), url1, Toast.LENGTH_LONG).show();

        // HttpAsyncTask().cancel(true);
        new HttpAsyncTask4().execute(url1);

          }

    private class HttpAsyncTask4 extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            return GET4(urls[0]);

        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                Toast.makeText(getBaseContext(), "Координаты переданы на сервер", Toast.LENGTH_SHORT).show();
                            }
            else {
                Toast.makeText(getBaseContext(), "Данные не переданы \n Проверьте подключение к интернет", Toast.LENGTH_LONG).show();
            }

        }


    }

    public static String GET4(String url){

        String result = null;
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            InputStream     inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }
    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line;
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }




    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }
    public void click1(View view) {
        // Do something in response to button
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.removeUpdates(this);
        stopUsingGPS();
        finish();

    }

    public void stopUsingGPS(){
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                if(locationManager != null){
            locationManager.removeUpdates(GPS_Location.this);
        }
    }
}