package com.logistoffice.autooffice;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;


public class status extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);

        // Get ListView object from xml
        final ListView listView = (ListView) findViewById(R.id.listView);

        // Defined Array values to show in ListView
        String[] values = new String[]{"Свободен",
                "В рейсе"};
        TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(this);
        final String imsiSIM1 = telephonyInfo.getImsiSIM1();
        // Define a new Adapter
        // First parameter - Context
        // Second parameter - Layout for the row
        // Third parameter - ID of the TextView to which the data is written
        // Forth - the Array of data

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);


        // Assign adapter to ListView
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

if (position == 1) {

    EditText city = (EditText) findViewById(R.id.editText7);
    CalendarView arrival = (CalendarView) findViewById(R.id.calendarView);
    Button save = (Button) findViewById(R.id.button4);

    city.setVisibility(View.VISIBLE);
    arrival.setVisibility(View.VISIBLE);
    save.setVisibility(View.VISIBLE);
}
                else {

    // ListView Clicked item value
    String itemValue = (String) listView.getItemAtPosition(position);

    // Show Alert
    Toast.makeText(getApplicationContext(),
            "Новый статус : " + itemValue, Toast.LENGTH_LONG)
            .show();

    try {
        String status1 = URLEncoder.encode(itemValue, "utf-8");
        String url = "http://logistoffice.com/gpsoffice/update_status.php?imei=" + imsiSIM1 + "&status=" + status1;
        new HttpAsyncTask3().execute(url);
    } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
    }


}
            }

        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_status, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static String GET3(String url) {

        String result = null;
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            InputStream  inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    // convert inputstream to String
    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public void save_status(View view) {
        TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(this);
        String imsiSIM1 = telephonyInfo.getImsiSIM1();
        ListView listView = (ListView) findViewById(R.id.listView);
        int position = 1;
        String itemValue = (String) listView.getItemAtPosition(position);
        EditText arrivalcity = (EditText) findViewById(R.id.editText7);
        CalendarView arrivaldate = (CalendarView) findViewById(R.id.calendarView);

        // Show Alert
        Toast.makeText(getApplicationContext(),
                "Новый статус : " + itemValue, Toast.LENGTH_LONG)
                .show();

        try {
            String status1 = URLEncoder.encode(itemValue, "utf-8");
            String arrivalcity1 = URLEncoder.encode(String.valueOf(arrivalcity.getText()), "utf-8");
            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy"); //android.text.format.DateFormat.getDateFormat(getApplicationContext());
            String arrivaldate1 = URLEncoder.encode(dateFormat.format(arrivaldate.getDate()),"utf-8");

            String url = "http://logistoffice.com/gpsoffice/update_status.php?imei=" + imsiSIM1 + "&status=" + status1 + "&arrivalcity=" + arrivalcity1 + "&arrivaldate=" + arrivaldate1;
            new HttpAsyncTask3().execute(url);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }



    }


    // check network connection


    private class HttpAsyncTask3 extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            return GET3(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            //Toast.makeText(getBaseContext(), result, Toast.LENGTH_LONG).show();
            finish();

            //etResponse.setText(result);
        }

    }
}